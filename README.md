# Example plugin for HERD library

This repository contains a few classes that illustrate how to create a hardware-specific/application-specific plugin using the [HERD library](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-library). Specifically, it contains an example device class, `DummyDevice`, that:

 * registers 5 commands, implemented by the `AlignLinks`, `ConfigureRxMGTs`, `ConfigureTxMGTs`, `Reboot` and `Reset` classes; and
 * registers an example Finite State Machine (FSM), whose transitions consist of one or more of the commands, run in sequence


## Dependencies

The main dependency is the HERD library, which in turn requires the following:

 * Build utilities: make, CMake3
 * [boost](https://boost.org)
 * [log4cplus](https://github.com/log4cplus/log4cplus)
 * [yaml-cpp](https://github.com/jbeder/yaml-cpp)
 * [msgpack](https://github.com/msgpack/msgpack-c)
 * [cppzmq](https://github.com/zeromq/cppzmq)


## Build instructions

 1. Install the HERD library depdendencies listed above. For example, on CentOS7:
```
yum install gcc-c++ make cmake3 boost-devel log4cplus-devel yaml-cpp-devel msgpack-devel cppzmq-devel
```

 2. Ensure that the `herd-library` submodule has been checked out. If not, run `git submodule update --init`.

 3. Build HERD library, then the plugin
```
mkdir build
cd build
cmake3 -DBUILD_SHARED_LIBS=ON ../herd-library
make
cd ..
make
```


## Using this plugin with the control application

The HERD control application (implemented in the `herd-control-app` repository) is an executable that loads HERD plugins, and provides a network interface that allows remote applications to run the commands and FSM transitions procedures declared in those plugins. If run with `dummy.yml` as the configuration file, the control application will load the dummy plugin and create two devices (of type `swatch::dummy::DummyDevice`, named `x0` and `x1`):
```
source env.sh
./build/herd-control-app dummy.yml
```

While this application is running, you can control the dummy devices instantiated by this server (i.e. run their commands and FSM transitions) using `control-app-console.py`:
```
./herd-library/control-app-console.py
```
In this console you can:
 * list device, command and FSM information by typing `info`;
 * run commands by typing `run-command DEVICE_ID COMMAND_ID` (e.g. `run-command x0 reset`);
 * engage FSMs by typing `engage-fsm DEVICE_ID FSM_ID` (e.g. `engage-fsm x0 myFSM`); and
 * reset FSMs by typing `reset-fsm DEVICE_ID FSM_ID` (e.g. `reset-fsm x0 myFSM`); and
 * disengage FSMs by typing `disengage-fsm DEVICE_ID FSM_ID` (e.g. `disengage-fsm x0 myFSM`); and
 * run FSM transitions by typing `run-transition DEVICE_ID COMMAND_ID` (e.g. `run-transition x0 configure`)
