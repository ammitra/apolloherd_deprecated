
#ifndef __SWATCH_DUMMY_COMMANDS_CONFIGURETXMGTS_HPP__
#define __SWATCH_DUMMY_COMMANDS_CONFIGURETXMGTS_HPP__


#include "swatch/action/Command.hpp"


namespace swatch {
namespace dummy {
namespace commands {


class ConfigureTxMGTs : public action::Command {
public:
  ConfigureTxMGTs(const std::string& aId, action::ActionableObject& aActionable);
  ~ConfigureTxMGTs();

private:
  State code(const core::ParameterSet& aParams);
};


} // namespace commands
} // namespace dummy
} // namespace swatch

#endif /* __SWATCH_DUMMY_COMMANDS_CONFIGURETXMGTS_HPP__ */
