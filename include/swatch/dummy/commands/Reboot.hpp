
#ifndef __SWATCH_DUMMY_COMMANDS_REBOOT_HPP__
#define __SWATCH_DUMMY_COMMANDS_REBOOT_HPP__


#include "swatch/action/Command.hpp"


namespace swatch {
namespace dummy {
namespace commands {


class Reboot : public action::Command {
public:
  Reboot(const std::string& aId, action::ActionableObject& aActionable);
  ~Reboot();

private:
  State code(const core::ParameterSet& aParams);
};


} // namespace commands
} // namespace dummy
} // namespace swatch

#endif /* __SWATCH_DUMMY_COMMANDS_REBOOT_HPP__ */
