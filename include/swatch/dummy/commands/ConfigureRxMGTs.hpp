
#ifndef __SWATCH_DUMMY_COMMANDS_CONFIGURERXMGTS_HPP__
#define __SWATCH_DUMMY_COMMANDS_CONFIGURERXMGTS_HPP__


#include "swatch/action/Command.hpp"


namespace swatch {
namespace dummy {
namespace commands {


class ConfigureRxMGTs : public action::Command {
public:
  ConfigureRxMGTs(const std::string& aId, action::ActionableObject& aActionable);
  ~ConfigureRxMGTs();

private:
  State code(const core::ParameterSet& aParams);
};


} // namespace commands
} // namespace dummy
} // namespace swatch

#endif /* __SWATCH_DUMMY_COMMANDS_CONFIGURERXMGTS_HPP__ */
