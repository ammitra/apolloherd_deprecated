
#ifndef __SWATCH_DUMMY_COMMANDS_ALIGNLINKS_HPP__
#define __SWATCH_DUMMY_COMMANDS_ALIGNLINKS_HPP__


#include "swatch/action/Command.hpp"


namespace swatch {
namespace dummy {
namespace commands {


class AlignLinks : public action::Command {
public:
  AlignLinks(const std::string& aId, action::ActionableObject& aActionable);
  ~AlignLinks();

private:
  State code(const core::ParameterSet& aParams);
};


} // namespace commands
} // namespace dummy
} // namespace swatch

#endif /* __SWATCH_DUMMY_COMMANDS_ALIGNLINKS_HPP__ */
