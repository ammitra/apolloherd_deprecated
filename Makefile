ROOT_DIR= $(shell pwd)

BUILD_HOME = $(ROOT_DIR)
include ${ROOT_DIR}/herd-library/deprecated/mfCommonDefs.mk

Project = swatch
Package = dummy

PackagePath = $(ROOT_DIR)
PackageName = cactuscore-herd-dummy

Packager = Alessandro Thea

PACKAGE_VER_MAJOR = 1
PACKAGE_VER_MINOR = 2
PACKAGE_VER_PATCH = 2
PACKAGE_RELEASE = 1

Library = herd_dummy

Includes = \
	include  \
	${ROOT_DIR}/herd-library/include

LibraryPaths = \
	${ROOT_DIR}/build \
	${ROOT_DIR}/lib

Libraries = \
	herd

ExecutableLibraries = \
	log4cplus \
	boost_system \
	boost_filesystem \
	boost_regex \
	boost_thread \
	herd \
	dl

include ${ROOT_DIR}/herd-library/deprecated/mfRules.mk
include ${ROOT_DIR}/herd-library/deprecated/mfRPMRules.mk
