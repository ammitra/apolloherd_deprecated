
#include "swatch/dummy/commands/Reset.hpp"


#include <chrono>
#include <thread>

#include "swatch/core/rules/IsAmong.hpp"
#include "swatch/dummy/DummyDevice.hpp"


namespace swatch {
namespace dummy {
namespace commands {


Reset::Reset(const std::string& aId, action::ActionableObject& aActionable) :
  Command(aId, aActionable, std::string("Dummy command's default result!"))
{
  registerParameter<std::string>("clockSource", "internal", core::rules::IsAmong({"internal", "external"}));

  // Extra dummy parameters
  registerParameter("cmdDuration", uint32_t(5));
  registerParameter<bool>("returnWarning", false);
  registerParameter<bool>("returnError", false);
  registerParameter<bool>("throw", false);
}

Reset::~Reset()
{
}

action::Command::State Reset::code(const core::ParameterSet& aParams)
{
  DummyDeviceController& lController = getActionable<DummyDevice>().getController();

  if (aParams.get<bool>("throw"))
    throw core::RuntimeError("An exceptional error occurred!");

  State lState = kDone;
  if (aParams.get<bool>("returnError"))
    lState = kError;
  else if (aParams.get<bool>("returnWarning"))
    lState = kWarning;
  else
    lController.reset();


  const size_t lNrSeconds = aParams.get<uint32_t>("cmdDuration");
  for (size_t i=0; i<(lNrSeconds*4); i++)
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(250));
    std::ostringstream lMsg;
    lMsg << "Done " << i+1 << " of " << (lNrSeconds*4) << " things";
    setProgress(float(i)/float(lNrSeconds*4), lMsg.str());
  }

  return lState;
}


} // namespace commands
} // namespace dummy
} // namespace swatch
